package my;

import java.util.ArrayList;

public class Printer {

    public void printArrayListArrayLists(ArrayList<ArrayList<int[]>> list){
        for (int i = 0; i < list.size(); i++) {
            ArrayList temp = list.get(i);
            for (int j = 0; j < temp.size(); j++) {
                int[] a = (int[]) temp.get(j);
                for (int k = 0; k < a.length; k++) {
                    System.out.print(a[k]);
                }
                System.out.println();
            }
            System.out.println();
        }
        System.out.println();
    }

    public void printIntArrayList(ArrayList<int[]> list){
        for (int i = 0; i < list.size(); i++) {
            int[] a = list.get(i);
            for (int j = 0; j < a.length; j++) {
                System.out.print(a[j]);
            }
            System.out.println();
        }
        System.out.println();
    }
}
