package my;

import java.util.ArrayList;

public class DoWork {
    public DoWork() {
        minimization();
    }

    private final Integer NUM_VARIABLE = 4;
    private final Integer[] FUNCTION = {0, 1, 2, 3, 4, 5, 6, 7, 11, 13, 14};

    private Printer printer = new Printer();
    private ArrayList<int[]> notGluing = new ArrayList();

    private void minimization() {
        ArrayList groups = createGroups();
        printer.printArrayListArrayLists(groups);
        System.out.println("---------------------");

        for (int i = 0; i < NUM_VARIABLE; i++) {
            groups = gluing(groups);
        }
        printer.printIntArrayList(notGluing);
    }

    private int[] convertToBinary(int dec){
        String str = Integer.toBinaryString(dec); //Integer to BinaryString
        char[] chr = str.toCharArray(); //convert to char Array
        if(chr.length > NUM_VARIABLE){
            System.out.println("Error, incorrect function value.");
            System.exit(0);
        }
        int[] bin = new int[NUM_VARIABLE]; //convert char Array to int Array
        for (int i = 0; i < NUM_VARIABLE; i++) {
            bin[i] = 0;
        }
        int temp = NUM_VARIABLE - 1;
        for (int i = chr.length - 1; i >= 0; i--) {
            bin[temp] = Character.digit(chr[i],2);
            temp--;
        }
        return bin; //return int Array
    }

    public ArrayList createGroups(){
        ArrayList<ArrayList<int[]>> groups = new ArrayList();
        for (int i = 0; i < NUM_VARIABLE + 1; i++) {
            groups.add(new ArrayList());
        }
        int counter = 0;
        for (int i = 0; i < FUNCTION.length; i++) {
            int[] temp = convertToBinary(FUNCTION[i]);
            for (int j = 0; j < temp.length; j++) {
                if (temp[j] == 1){
                    counter++;
                }
            }
            groups.get(counter).add(temp);
            counter = 0;
        }
        return groups;
    }

    public ArrayList gluing(ArrayList<ArrayList<int[]>> groups){
        ArrayList<int[]> allOne = createCheckArrayList(groups);
        ArrayList<ArrayList<int[]>> newGroups = new ArrayList();
        for (int i = 0; i < groups.size(); i++) {
            ArrayList<int[]> first = groups.get(i);
            if (first.size() != 0) {
                for (int j = i + 1; j < groups.size(); j++) {
                    ArrayList<int[]> second = groups.get(j);
                    if (second.size() != 0) {
                        newGroups.add(gluingGroups(first, second, allOne));
                        break;
                    }
                }
            }
        }
        for (int[] i: allOne) {
            notGluing.add(i);
        }
        return newGroups;
    }

    private ArrayList createCheckArrayList(ArrayList<ArrayList<int[]>> groups){
        ArrayList allOne = new ArrayList();
        for (ArrayList i: groups) {
            ArrayList<int[]> inside = i;
            for (int[] j: inside) {
                allOne.add(j);
            }
        }

        return allOne;
    }

    private ArrayList<int[]> gluingGroups(ArrayList<int[]> first, ArrayList<int[]> second, ArrayList<int[]> allOne){
        ArrayList<int[]> insideGroup = new ArrayList();
        for (int i = 0; i < first.size(); i++) {
            for (int j = 0; j < second.size(); j++) {
                if (compareIntArrays(first.get(i), second.get(j))) {
                    searchNotGluing(first.get(i), second.get(j), allOne);
                    int[] newArray = gluingIntArrays(first.get(i), second.get(j));
                    int countCopies = checkCopies(insideGroup, newArray);
                    if (countCopies == 0) {
                        insideGroup.add(newArray);
                    }
                }
            }
        }
        return insideGroup;
    }

    private void searchNotGluing(int[] first, int[] second, ArrayList<int[]> allOne){
        int count = 0;
        int index = 0;
        for (int i = 0; i < allOne.size(); i++) {
            if (compareIntArraysFull(first, allOne.get(i))){
                index = i;
                count++;
            }
        }
        if (count != 0) {
            allOne.remove(index);
            index = 0;
            count = 0;
        }
        for (int i = 0; i < allOne.size(); i++) {
            if (compareIntArraysFull(second, allOne.get(i))){
                index = i;
                count++;
            }
        }
        if (count != 0) {
            allOne.remove(index);
        }
    }

    private boolean compareIntArraysFull(int[] first, int[] second){
        boolean solution = true;
        int counter = 0;
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]){
                counter++;
            }
            if (counter != 0){
                solution = false;
                break;
            }
        }
        return solution;
    }

    private boolean compareIntArrays(int[] first, int[] second){
        boolean solution = true;
        int counter = 0;
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]){
                counter++;
            }
            if (counter > 1){
                solution = false;
                break;
            }
        }
        return solution;
    }

    private int[] gluingIntArrays(int[] first, int[] second){
        int[] newArray = new int[first.length];
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]){
                newArray[i] = 7;
            }else newArray[i] = first[i];
        }
        return newArray;
    }

    private int checkCopies(ArrayList<int[]> own, int[] noOwn){
        int countCopies = 0;
        for (int[] iter : own) {
            if (compareIntArrays(iter, noOwn)) {
                countCopies++;
            }
        }
        return countCopies;
    }
}
